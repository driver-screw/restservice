package com.my.web.service.serevice;

import com.my.web.service.dao.BookDAO;
import com.my.web.service.entity.Book;
import com.my.web.service.rest.exceptioin.ObjectNotFoundException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Service
@Transactional
public class BookServiceImpl implements BookService {
    static Logger logger = LogManager.getLogger(BookServiceImpl.class);

    @Autowired
    private BookDAO bookDAO;

    @Override
    public List<Book> getBooks() {
        logger.trace(() -> "BookService invoke BookDAO");
        List<Book> books = bookDAO.getBooks();
        return books == null ? new ArrayList<>() : books;
    }

    @Override
    public void saveBook(Book book) {
        logger.trace(() -> "BookService invoke BookDAO to save Book");
        bookDAO.saveBook(book);
    }

    @Override
    public Book getBook(int id) {
        logger.trace(() -> "BookService invoke BookDAO get by id");
        Book book = bookDAO.getBook(id);
        if (Objects.isNull(book)) {
            throw new ObjectNotFoundException(String.format("Book with id=%s is not found.", id));
        }
        return book;
    }

    @Override
    public void deleteBook(int id) {
        logger.trace(() -> "BookService invoke BookDAO deleteBook");
        bookDAO.deleteBook(id);
    }
}
