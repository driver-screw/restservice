package com.my.web.service.serevice;

import com.my.web.service.dao.AuthorDAO;
import com.my.web.service.entity.Author;
import com.my.web.service.rest.exceptioin.ObjectNotFoundException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Service
@Transactional
public class AuthorServiceImpl implements AuthorService {
    static Logger logger = LogManager.getLogger(AuthorServiceImpl.class);

    @Autowired
    private AuthorDAO authorDAO;

    @Override
    public List<Author> getAuthors() {
        logger.trace(() -> "AuthorService invoke AuthorDAO");
        List<Author> authors = authorDAO.getAuthors();
        return authors == null ? new ArrayList<>() : authors;
    }

    @Override
    public void saveAuthor(Author author) {
        logger.trace(() -> "AuthorService invoke AuthorDAO to save Author");
        authorDAO.saveAuthor(author);
    }

    @Override
    public Author getAuthor(int id) {
        logger.trace(() -> "AuthorService invoke AuthorDAO get by id");
        Author author = authorDAO.getAuthor(id);
        if (Objects.isNull(author)) {
            throw new ObjectNotFoundException(String.format("Author with id=%s is not found.", id));
        }
        return author;
    }

    @Override
    public void deleteAuthor(int id) {
        logger.trace(() -> "AuthorService invoke AuthorDAO deleteBook");
        authorDAO.deleteAuthor(id);
    }
}
