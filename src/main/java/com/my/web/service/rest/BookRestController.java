package com.my.web.service.rest;

import com.my.web.service.entity.Author;
import com.my.web.service.entity.Book;
import com.my.web.service.serevice.BookService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/library")
public class BookRestController {
    static Logger logger = LogManager.getLogger(BookRestController.class);


    @Autowired
    private BookService bookService;

    @GetMapping("/books")
    public List<Book> getBooks(@RequestParam(required = false) String authorLastName) {
        logger.debug(() -> "BookRestController mapped request getBooks");
        List<Book> books = bookService.getBooks();
        if (Objects.isNull(authorLastName)) {
            return books;
        } else return books.stream().filter(book -> {
            List<Author> authors = book.getAuthors();
            Optional<Author> first = authors.stream().filter(author -> author.getLastName().equalsIgnoreCase(authorLastName)).findFirst();
            return first.isPresent();
        }).collect(Collectors.toList());
    }


    @GetMapping("/books/{bookId}")
    public Book getBook(@PathVariable int bookId) {
        logger.debug(() -> "BookRestController mapped request getBook");
        return bookService.getBook(bookId);
    }

    @PostMapping("/books")
    @ResponseStatus(value = HttpStatus.CREATED)
    public Book addBook(@RequestBody Book book) {
        logger.debug(() -> "BookRestController mapped request addBook");
        book.setId(0);
        bookService.saveBook(book);
        return book;
    }

    @PutMapping("/books/{bookId}")
    public Book updateBook(@PathVariable int bookId, @RequestBody Book book) {
        logger.debug(() -> "BookRestController mapped request addBook");
        book.setId(bookId);
        bookService.saveBook(book);
        return book;
    }

    @DeleteMapping("/books/{bookId}")
    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    public void deleteBook(@PathVariable int bookId) {
        logger.debug(() -> "BookRestController mapped request getBook");
        bookService.getBook(bookId);
        bookService.deleteBook(bookId);
    }


}
