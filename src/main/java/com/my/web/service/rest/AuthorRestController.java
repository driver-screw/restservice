package com.my.web.service.rest;

import com.my.web.service.entity.Author;
import com.my.web.service.serevice.AuthorService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/library")
public class AuthorRestController {
    static Logger logger = LogManager.getLogger(AuthorRestController.class);


    @Autowired
    private AuthorService authorService;

    @GetMapping("/authors")
    public List<Author> getAuthors() {
        return authorService.getAuthors();

    }


    @GetMapping("/authors/{authorId}")
    public Author getAuthor(@PathVariable int authorId) {
        return authorService.getAuthor(authorId);
    }

    @PostMapping("/authors")
    @ResponseStatus(value = HttpStatus.CREATED)
    public Author addBook(@RequestBody Author author) {
        author.setId(0);
        authorService.saveAuthor(author);
        return author;
    }

    @PutMapping("/authors/{authorId}")
    public Author updateBook(@PathVariable int authorId, @RequestBody Author author) {
        author.setId(authorId);
        authorService.saveAuthor(author);
        return author;
    }

    @DeleteMapping("/authors/{authorId}")
    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    public void deleteBook(@PathVariable int authorId) {
        authorService.getAuthor(authorId);
        authorService.deleteAuthor(authorId);
    }


}
