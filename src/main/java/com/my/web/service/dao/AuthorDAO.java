package com.my.web.service.dao;

import com.my.web.service.entity.Author;

import java.util.List;

public interface AuthorDAO {
    List<Author> getAuthors();

    void saveAuthor(Author author);

    Author getAuthor(int id);

    void deleteAuthor(int id);
}
