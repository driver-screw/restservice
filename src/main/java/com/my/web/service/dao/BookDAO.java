package com.my.web.service.dao;

import com.my.web.service.entity.Book;

import java.util.List;

public interface BookDAO {
    List<Book> getBooks();

    void saveBook(Book book);

    Book getBook(int id);

    void deleteBook(int id);
}
