package com.my.web.service.dao;

import com.my.web.service.entity.Author;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class AuthorDaoImpl implements AuthorDAO {

    static Logger logger = LogManager.getLogger(AuthorDaoImpl.class);


    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public List<Author> getAuthors() {
        Session session = sessionFactory.getCurrentSession();
        Query<Author> query = session.createQuery("from Author order by id", Author.class);
        logger.debug(() -> "Executed query: \"from Author order by name\"");

        return query.getResultList();
    }

    @Override
    public void saveAuthor(Author author) {
        Session session = sessionFactory.getCurrentSession();
        session.saveOrUpdate(author);
    }

    @Override
    public Author getAuthor(int id) {
        Session session = sessionFactory.getCurrentSession();
        Author author = session.get(Author.class, id);
        logger.debug(() -> "Get Author with id");
        return author;
    }

    @Override
    public void deleteAuthor(int id) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("delete from Author where id=:authorId");
        query.setParameter("authorId", id);
        query.executeUpdate();
    }
}
