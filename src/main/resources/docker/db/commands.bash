# Run from the same directory as script is located

# copy sql scripts
cp ../../sqlScripts/* ./

# build image
docker build -t web-mysql-db ./

#create network
docker network create app-network

# run db container
docker run -d --name web-mysql-db-container \
 --network app-network --network-alias mysql-db \
  --platform linux/x86_64 -p 3306:3306 web-mysql-db \
  --mysql-native-password=ON
