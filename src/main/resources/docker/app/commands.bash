#build project to create .war file. To ignore database test may be required. Possible will be fixed in the future
cd ../../../../../
mvn clean
mvn package

# copy .war file
cd src/main/resources/docker/app
cp ../../sqlScripts/* ./

# build image
 docker build -t my-rest-service ./src/main/resources/docker/app/

# run app container
docker run -d --name my-rest-service-container --network app-network -e jdbc.url='jdbc:mysql://mysql-db:3306/my-web-service?useSSL=false&serverTimezone=UTC' -v /Users/anatoliipiddubnyi/mytmp/logback.xml:/home/user/che/tomcat/conf/logback.xml --platform linux/x86_64 -p 8080:8080 my-rest-service
