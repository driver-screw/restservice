-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema my-web-service
-- -----------------------------------------------------
drop SCHEMA IF EXISTS `my-web-service` ;

-- -----------------------------------------------------
-- Schema my-web-service
-- -----------------------------------------------------
create SCHEMA IF NOT EXISTS `my-web-service` DEFAULT CHARACTER SET latin1 ;
USE `my-web-service` ;

-- -----------------------------------------------------
-- Table `my-web-service`.`book`
-- -----------------------------------------------------
drop table IF EXISTS `my-web-service`.`book` ;

create TABLE IF NOT EXISTS `my-web-service`.`book` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NULL,
  `description` VARCHAR(45) NULL,
  `genre` VARCHAR(45) NULL,
  `available` INT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC) VISIBLE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `my-web-service`.`author`
-- -----------------------------------------------------
drop table IF EXISTS `my-web-service`.`author` ;

create TABLE IF NOT EXISTS `my-web-service`.`author` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `first_name` VARCHAR(45) NOT NULL,
  `last_name` VARCHAR(45) NOT NULL,
  `middle_name` VARCHAR(45) NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC) VISIBLE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `my-web-service`.`book_has_author`
-- -----------------------------------------------------
drop table IF EXISTS `my-web-service`.`book_has_author` ;

create TABLE IF NOT EXISTS `my-web-service`.`book_has_author` (
  `book_id` INT NOT NULL,
  `author_id` INT NOT NULL,
  PRIMARY KEY (`book_id`, `author_id`),
  INDEX `fk_book_has_author_author1_idx` (`author_id` ASC) VISIBLE,
  INDEX `fk_book_has_author_book_idx` (`book_id` ASC) VISIBLE,
  CONSTRAINT `fk_book_has_author_book`
    FOREIGN KEY (`book_id`)
    REFERENCES `my-web-service`.`book` (`id`)
    ON delete NO ACTION
    ON update NO ACTION,
  CONSTRAINT `fk_book_has_author_author1`
    FOREIGN KEY (`author_id`)
    REFERENCES `my-web-service`.`author` (`id`)
    ON delete NO ACTION
    ON update NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

LOCK TABLES `author` WRITE;
/*!40000 ALTER TABLE `author` DISABLE KEYS */;
insert into `author` VALUES
	(1,'David','Adams',null),
	(2,'John','Doe',null),
	(3,'Ajay','Rao',null),
	(4,'Mary','Public',null),
	(5,'Maxwell','Dixon','Philipovich');
UNLOCK TABLES;

LOCK TABLES `book` WRITE;
/*!40000 ALTER TABLE `book` DISABLE KEYS */;
insert into `book` VALUES
	(1,'A little book of calm','How to keep calm','lifestyle',10);
/*!40000 ALTER TABLE `book` ENABLE KEYS */;
UNLOCK TABLES;

LOCK TABLES `book_has_author` WRITE;
/*!40000 ALTER TABLE `book_has_author` DISABLE KEYS */;
insert into `book_has_author` VALUES
	(1,1),
    (1,5);
/*!40000 ALTER TABLE `book_has_author` ENABLE KEYS */;
UNLOCK TABLES;
