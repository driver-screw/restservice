CREATE USER 'webserviceuser'@'localhost' IDENTIFIED BY 'webserviceuser';

GRANT ALL PRIVILEGES ON * . * TO 'webserviceuser'@'localhost';

ALTER USER 'webserviceuser'@'localhost' IDENTIFIED WITH mysql_native_password BY 'webserviceuser';

CREATE USER 'webserviceuser'@'%' IDENTIFIED BY 'webserviceuser';

GRANT ALL PRIVILEGES ON * . * TO 'webserviceuser'@'%';

ALTER USER 'webserviceuser'@'%' IDENTIFIED WITH mysql_native_password BY 'webserviceuser';