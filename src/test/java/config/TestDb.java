package config;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Assert;
import org.junit.Test;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

@WebServlet("/TestDbServlet")
public class TestDb extends HttpServlet {
    static Logger logger = LogManager.getLogger(TestDb.class);


    @Test
    public void testDbConnection() {
        String user = "webserviceuser";
        String pass = "webserviceuser";
        logger.info(() -> "Info log");
        logger.error(() -> "Error log");
        logger.debug(() -> "Debug log");
        logger.warn(() -> "Warn log");
        logger.fatal(() -> "Fatal log");
        logger.trace(() -> "Trace log");

        String jdbcUrl = "jdbc:mysql://localhost:3306/my-web-service?useSSL=false&serverTimezone=UTC";
        // Loading class `com.mysql.jdbc.Driver'. This is deprecated.
        // The new driver class is `com.mysql.cj.jdbc.Driver'.
        // The driver is automatically registered via the SPI and manual loading of the driver class is generally unnecessary.
        String driver = "com.mysql.cj.jdbc.Driver";
        logger.debug(() -> "Connecting to database: " + jdbcUrl);
        try (Connection conn = DriverManager.getConnection(jdbcUrl, user, pass)) {
            Class.forName(driver);
            logger.debug(() -> "Connection success");
        } catch (ClassNotFoundException | SQLException e) {
            Assert.fail();
        }

    }
}
